﻿using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField] private GameController _gameController;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _gameController = FindObjectOfType<GameController>();
    }
#endif

    public void SpawnEnemy(EnemyController enemyPrefab)
    {
        var enemy = Instantiate(enemyPrefab.gameObject, transform.position, Quaternion.Euler(enemyPrefab.SpawnRotation))
            .GetComponent<EnemyController>();
        enemy.GameController = _gameController;
    }
}