﻿using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private string _name;
    [SerializeField] private float _speed;
    [SerializeField] private int _hitsToTranquilize;
    [SerializeField] private Vector3 _spawnRotation;
    [SerializeField] [HideInInspector] private int _arrowLayer, _gunLayer;

    private int _hitCount;

    public GameController GameController { private get; set; }

    public Vector3 SpawnRotation
    {
        get { return _spawnRotation; }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        _arrowLayer = LayerMask.NameToLayer("Arrow");
        _gunLayer = LayerMask.NameToLayer("Gun");
        gameObject.name = _name;
    }
#endif

    private void Update()
    {
        if (GameController != null)
            transform.position += Vector3.down * GameController.ComputeSpeed(_speed) * Time.deltaTime;
    }

    private void Tranquilize()
    {
        if (GameController == null) return;
        Destroy(gameObject);
        GameController.Score++;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (GameController == null) return;
        if (other.gameObject.layer == _arrowLayer)
        {
            Destroy(other.gameObject);
            _hitCount++;
            if (_hitCount >= _hitsToTranquilize)
                Tranquilize();
        }
        else if (other.gameObject.layer == _gunLayer)
            GameController.GameOver();
    }
}