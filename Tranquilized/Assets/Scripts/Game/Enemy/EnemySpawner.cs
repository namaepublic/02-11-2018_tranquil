﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private SpawnController[] _spawns;
    [SerializeField] private EnemyController[] _enemiesPrefabs;
    [SerializeField] private Vector2 _timerRange;

    [SerializeField] private float _timer, _currentTime;
    
    private bool _init;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _spawns = GetComponentsInChildren<SpawnController>();
    }
#endif
    
    public void Init(bool value)
    {
        _timer = Random.Range(_timerRange.x, _timerRange.y);
        _init = value;
    }
    
    private void Update()
    {
        if (!_init) return;
        
        if (_currentTime <= _timer)
            _currentTime += Time.deltaTime;
        else
            SpawnRandomEnemy();
    }

    private void SpawnRandomEnemy()
    {
        var enemyIndex = Random.Range(0, _enemiesPrefabs.Length);
        var spawnIndex = Random.Range(0, _spawns.Length);
        _spawns[spawnIndex].SpawnEnemy(_enemiesPrefabs[enemyIndex]);
        _currentTime = 0;
        _timer = Random.Range(_timerRange.x, _timerRange.y);
    }
}