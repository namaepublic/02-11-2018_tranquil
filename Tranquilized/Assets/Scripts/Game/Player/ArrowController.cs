﻿using UnityEngine;

public class ArrowController : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] [HideInInspector] private int _spawnLayer;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _spawnLayer = LayerMask.NameToLayer("Spawn");
    }
#endif

    private void Update()
    {
        transform.position += Vector3.up * _speed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == _spawnLayer)
            Destroy(gameObject);
    }
}