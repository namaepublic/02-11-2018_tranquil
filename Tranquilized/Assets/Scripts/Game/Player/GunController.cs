﻿using UnityEngine;

public class GunController : MonoBehaviour
{
    [SerializeField] private ArrowController _arrowPrefab;
    [SerializeField] private SpriteRenderer _arrowSprite;
    [SerializeField] private float _cooldown;

    [SerializeField] private float _currentTime;
    [SerializeField] private bool _canShoot = true;
    
    private bool _init;

    public void Init(bool value)
    {
        _canShoot = value;
        _currentTime = 0;
        _init = value;
    }
    
    private void OnMouseDown()
    {
        if (_init && _canShoot) ShootArrow();
    }

    private void Update()
    {
        if (!_init) return;
        
        if (_currentTime < _cooldown)
            _currentTime += Time.deltaTime;
        else
        {
            _canShoot = true;
            _arrowSprite.enabled = true;
        }
    }

    private void ShootArrow()
    {
        Instantiate(_arrowPrefab.gameObject, transform.position, Quaternion.Euler(0, 0, 90));
        _canShoot = false;
        _currentTime = 0;
        _arrowSprite.enabled = false;
    }
}