﻿using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private GameObject _menuCanvas, _scoreCanvas;
    [SerializeField] private AnimationCurve _speedMultiplier;
    [SerializeField] private GunController[] _gunControllers;
    [SerializeField] private EnemySpawner _enemySpawner;

    private int _score;
    
#if UNITY_EDITOR
    private void OnValidate()
    {
        _gunControllers = FindObjectsOfType<GunController>();
        _enemySpawner = FindObjectOfType<EnemySpawner>();
    }
#endif

    private void Awake()
    {
        Init(false);
    }

    public void Init(bool value)
    {
        _menuCanvas.SetActive(!value);
        foreach (var gunController in _gunControllers)
            gunController.Init(value);
        _enemySpawner.Init(value);
        _scoreCanvas.SetActive(value);
    }

    public void StartGame()
    {
        Score = 0;
        Init(true);
    }

    public int Score
    {
        get { return _score; }
        set
        {
            _score = value;
            _scoreText.text = _score.ToString();
        }
    }

    public float ComputeSpeed(float speed)
    {
        return _speedMultiplier.Evaluate(_score) * speed;
    }

    public void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}